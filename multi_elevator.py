import sys
import json

from main.model import View, Scheduler, Cabin, Direction
from main.multi_controller import MultiController
from schema import Schema, And, Use


MAXIMUM_NUMBER_OF_FLOORS = 100
MAXIMUM_NUMBER_OF_TICKS = 500


def validate(data):

    schema = Schema({
        "numberOfFloors": And(Use(int), lambda n: 0 < n < MAXIMUM_NUMBER_OF_FLOORS),
        "numberOfTicks": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_TICKS),
        "external": Schema([{
            "tick": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_FLOORS),
            "floor": And(Use(int), lambda n: 0 <= n < data["numberOfFloors"]),
            "direction":  And(Use(str), lambda s: s in ("UP", "DOWN"))
        }]),
        "cabins": Schema([{
            "currentPosition": And(Use(int), lambda n: 0 < n < data["numberOfFloors"]),
            "internal": Schema([{
                "tick": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_FLOORS),
                "floor": And(Use(int), lambda n: 0 <= n < data["numberOfFloors"])
            }])
        }])
    })

    return schema.validate(data)


if __name__ == '__main__':
    file_name = sys.argv[1] if len(sys.argv) > 1 else "multi.json"
    print("Reading from file: %s" % file_name)

    with open(file_name, 'r') as f:
        raw_data = f.read()

    data = json.loads(raw_data)

    validated = validate(data)

    view = View.of()
    scheduler = Scheduler.of()
    cabins = [Cabin.of(validated["numberOfFloors"], cabin["currentPosition"], scheduler) for cabin in validated["cabins"]]
    controller = MultiController(cabins, view, validated["numberOfTicks"])

    for er in validated["external"]:
        print("-------------------- external ----------------------")
        print("Tick(time): %s" % er["tick"])
        print("On floor: %s" % er["floor"])
        print("Direction: %s" % er["direction"])
        print("")
        controller.external_request(er["tick"], er["floor"], Direction.from_str(er["direction"]))

    for idx, cbn in enumerate(validated["cabins"]):
        for ir in cbn["internal"]:
            print("-------------------- internal ----------------------")
            print("Tick(time): %s" % ir["tick"])
            print("To floor: %s" % ir["floor"])
            print("")
            controller.internal_request(ir["tick"], ir["floor"], idx)

    controller.run()