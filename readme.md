## Implementation of Elevator
All re requirements are implemented including multiple elevators and optional items

#### How to install
```
pip install -r requirements.txt
```
#### How to run tests
```
python3 -m unittest discover test
```
#### How to run application
Application accepts input in json format. Please look at `multi.json` for the reference.    
To run application execute the command:
```
python3 multi_elevator.py multi.json
```
