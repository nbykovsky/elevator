import json
from schema import Schema, And, Use
import sys

from main.controller import Controller
from main.model import View, Scheduler, Cabin

MAXIMUM_NUMBER_OF_FLOORS = 100
MAXIMUM_NUMBER_OF_TICKS = 500


def validate(data):

    schema = Schema({
        "numberOfFloors": And(Use(int), lambda n: 0 < n < MAXIMUM_NUMBER_OF_FLOORS),
        "currentPosition": And(Use(int), lambda n: 0 < n < data["numberOfFloors"]),
        "numberOfTicks": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_TICKS),
        "internal": Schema([{
            "tick": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_FLOORS),
            "floor": And(Use(int), lambda n: 0 <= n < data["numberOfFloors"])
        }])
        ,
        "external": Schema([{
            "tick": And(Use(int), lambda n: 0 <= n < MAXIMUM_NUMBER_OF_FLOORS),
            "floor": And(Use(int), lambda n: 0 <= n < data["numberOfFloors"]),
            "direction":  And(Use(str), lambda s: s in ("UP", "DOWN"))
        }])
    })

    return schema.validate(data)


if __name__ == '__main__':
    file_name = sys.argv[1] if len(sys.argv) > 1 else "input.json"
    print("Reading from file: %s" % file_name)

    with open(file_name, 'r') as f:
        raw_data = f.read()

    data = json.loads(raw_data)
    validated = validate(data)

    print("Number of floors: %s" % validated["numberOfFloors"])
    print("Current floor: %s" % validated["currentPosition"])
    print("Number of ticks %s" % validated["numberOfTicks"])

    view = View.of()
    scheduler = Scheduler.of()
    cabin = Cabin.of(validated["numberOfFloors"], validated["currentPosition"], scheduler)
    controller = Controller(cabin, view, validated["numberOfTicks"])

    for ir in validated["internal"]:
        print("-------------------- internal ----------------------")
        print("Tick(time): %s" % ir["tick"])
        print("Ordered floor: %s" % ir["floor"])
        print("")
        controller.internal_request(ir["tick"], ir["floor"])

    for er in validated["external"]:
        print("-------------------- internal ----------------------")
        print("Tick(time): %s" % er["tick"])
        print("Waiting on floor: %s" % er["floor"])
        print("Direction: %s" % er["direction"])
        print("")
        controller.external_request(er["tick"], er["floor"], er["direction"])

    controller.run()

