import unittest
from typing import Dict, Tuple, List

from main.controller import Controller
from main.model import View, Cabin, Direction, State, Scheduler, CabinState, DoorState


class DummyView(View):

    def __init__(self, expected: Dict[int, CabinState]):
        self._expected = expected

    def show(self, state: CabinState, tick: int):
        ex_state = self._expected[tick]
        assert ex_state.floor == state.floor
        assert ex_state.direction == state.direction
        assert ex_state.doors == state.doors
        return []

    def show_many(self, states: List[CabinState], tick: int):
        pass


class TestsController(unittest.TestCase):

    def test_controller(self):

        view = DummyView({
            0: CabinState(Direction.UP, 4, DoorState.CLOSED),
            1: CabinState(Direction.UP, 5, DoorState.CLOSED),
            2: CabinState(Direction.UP, 5, DoorState.OPEN),
            3: CabinState(Direction.UP, 5, DoorState.CLOSED),
            4: CabinState(Direction.UP, 6, DoorState.CLOSED),
            5: CabinState(Direction.UP, 6, DoorState.OPEN),
            6: CabinState(Direction.UP, 6, DoorState.CLOSED),
            7: CabinState(Direction.DOWN, 5, DoorState.CLOSED),
            8: CabinState(Direction.DOWN, 4, DoorState.CLOSED),
            9: CabinState(Direction.DOWN, 3, DoorState.CLOSED),
            10: CabinState(Direction.DOWN, 3, DoorState.OPEN),
            11: CabinState(Direction.DOWN, 3, DoorState.CLOSED),
            12: CabinState(Direction.NONE, 2, DoorState.CLOSED)
        })

        scheduler = Scheduler.of()
        cabin = Cabin.of(9, 3, scheduler)
        controller = Controller(cabin, view, 12)

        controller.internal_request(0, 5)
        controller.internal_request(1, 6)
        controller.external_request(1, 3, Direction.UP)

        controller.run()


