import unittest
from collections import deque
from typing import Set, Tuple, List

from main.cabin_impl import CabinImpl
from main.model import Scheduler, Direction, State, Request, CabinState, DoorState


class DummyScheduler(Scheduler):
    def calculate_route(self, state: CabinState,
                        requests: Set[Request]) -> List[Tuple[State, Set[Request]]]:
        return []


class TestCabinImpl(unittest.TestCase):

    def test_cabin_impl_dummy(self):
        scheduler = DummyScheduler()
        cabin = CabinImpl(10, 4, scheduler)

        cabin.add_request(Request(4, Direction.NONE))
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE)})
        self.assertEqual(cabin.state.direction, Direction.NONE)

        cabin.add_request(Request(5, Direction.NONE))
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE), Request(5, Direction.NONE)})
        self.assertEqual(cabin.state.direction, Direction.UP)

        cabin.add_request(Request(5, Direction.DOWN))
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE), Request(5, Direction.DOWN), Request(5, Direction.NONE)})
        self.assertEqual(cabin.state.direction, Direction.UP)

        cabin = CabinImpl(10, 4, scheduler)
        cabin._state.doors = DoorState.OPEN
        cabin.add_request(Request(4, Direction.NONE))
        self.assertEqual(cabin.state.direction, Direction.NONE)
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE)})
        cabin._state.direction = Direction.UP
        cabin.add_request(Request(4, Direction.UP))
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE), Request(4, Direction.UP)})
        cabin.add_request(Request(4, Direction.DOWN))
        self.assertEqual(cabin._requests, {Request(4, Direction.NONE), Request(4, Direction.UP),
                                           Request(4, Direction.DOWN)})

    def test_cabin_impl_ticks(self):
        scheduler = DummyScheduler()
        cabin = CabinImpl(10, 4, scheduler)
        req = {
            Request(4, Direction.UP),
            Request(3, Direction.DOWN),
            Request(7, Direction.UP),
            Request(1, Direction.NONE),
        }
        cabin._requests = req
        cabin._route = list(reversed([
            (State.UPWARD, {Request(7, Direction.UP)}),
            (State.DOWNWARD, {Request(4, Direction.UP)}),
            (State.UPWARD, {Request(1, Direction.NONE)}),
            (State.DOWNWARD, set()),
            (State.UPWARD, {Request(3, Direction.DOWN)}),
        ]))

        self.assertEqual(cabin.state.floor, 4)
        self.assertEqual(cabin.state.direction, Direction.NONE)
        self.assertEqual(cabin._requests, req)

        cabin.tick()
        self.assertEqual(cabin.state.floor, 5)
        self.assertEqual(cabin.state.direction, Direction.UP)
        self.assertEqual(cabin._requests, req - {Request(7, Direction.UP)})

        cabin.tick()
        self.assertEqual(cabin.state.floor, 4)
        self.assertEqual(cabin.state.direction, Direction.DOWN)
        self.assertEqual(cabin._requests, req - {Request(7, Direction.UP), Request(4, Direction.UP)})

        cabin.tick()
        self.assertEqual(cabin.state.floor, 5)
        self.assertEqual(cabin.state.direction, Direction.UP)
        self.assertEqual(cabin._requests, req - {Request(7, Direction.UP),
                                                 Request(4, Direction.UP), Request(1, Direction.NONE)})

        cabin.tick()
        self.assertEqual(cabin.state.floor, 4)
        self.assertEqual(cabin.state.direction, Direction.DOWN)
        self.assertEqual(cabin._requests, req - {Request(7, Direction.UP),
                                                 Request(4, Direction.UP), Request(1, Direction.NONE)})

        cabin.tick()
        self.assertEqual(cabin.state.floor, 5)
        self.assertEqual(cabin.state.direction, Direction.UP)
        self.assertEqual(cabin._requests, req - {Request(7, Direction.UP),
                                                 Request(4, Direction.UP), Request(1, Direction.NONE),
                                                 Request(3, Direction.DOWN)})


class NotDummyScheduler(Scheduler):
    def calculate_route(self, state: CabinState,
                        requests: Set[Request]) -> List[Tuple[State, Set[Request]]]:
        return [
            (State.UPWARD, {Request(7, Direction.UP)}),
            (State.DOWNWARD, {Request(4, Direction.UP)}),
            (State.UPWARD, {Request(1, Direction.NONE)}),
            (State.DOWNWARD, set()),
            (State.UPWARD, {Request(3, Direction.DOWN)}),
        ]


class TestCabinImpl1(unittest.TestCase):

    def test_cabin_try(self):
        scheduler = NotDummyScheduler()
        cabin = CabinImpl(10, 4, scheduler)
        req = {
            Request(4, Direction.UP),
            Request(3, Direction.DOWN),
            Request(7, Direction.UP),
            Request(1, Direction.NONE),
        }
        cabin._requests = req
        cabin._route = list(reversed([
            (State.UPWARD, {Request(7, Direction.UP)}),
            (State.DOWNWARD, {Request(4, Direction.UP)}),
            (State.UPWARD, {Request(1, Direction.NONE)}),
            (State.DOWNWARD, set()),
            (State.UPWARD, {Request(3, Direction.DOWN)}),
        ]))

        self.assertEqual(cabin.try_request(Request(7, Direction.UP)), 0)
        self.assertEqual(cabin.try_request(Request(4, Direction.UP)), 1)
        self.assertEqual(cabin.try_request(Request(1, Direction.NONE)), 2)
        self.assertEqual(cabin.try_request(Request(3, Direction.DOWN)), 4)
