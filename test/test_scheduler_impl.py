import unittest

from main.model import Direction, Request, State, CabinState, DoorState
from main.scheduler_impl import calc_floors, calc_states, SchedulerImpl


class TestCalcFloors(unittest.TestCase):

    def test_calc_floors(self):
        self.assertEqual(calc_floors(Direction.UP, 0, {
            Request(2, Direction.NONE)
        }), [
            Request(2, Direction.NONE)
        ])

        self.assertEqual(calc_floors(Direction.UP, 3, {
            Request(5, Direction.UP),
            Request(1, Direction.DOWN),
            Request(2, Direction.NONE),
            Request(4, Direction.NONE),
        }), [
            Request(4, Direction.NONE),
            Request(5, Direction.UP),
            Request(2, Direction.NONE),
            Request(1, Direction.DOWN)
        ])

        self.assertEqual(calc_floors(Direction.DOWN, 3, {
            Request(5, Direction.UP),
            Request(1, Direction.DOWN),
            Request(2, Direction.NONE),
            Request(4, Direction.NONE),
        }), [
            Request(2, Direction.NONE),
            Request(1, Direction.DOWN),
            Request(4, Direction.NONE),
            Request(5, Direction.UP)
        ])

        self.assertEqual(calc_floors(Direction.UP, 3, {
            Request(5, Direction.NONE),
            Request(1, Direction.NONE),
            Request(2, Direction.NONE),
            Request(4, Direction.NONE),
        }), [
            Request(4, Direction.NONE),
            Request(5, Direction.NONE),
            Request(2, Direction.NONE),
            Request(1, Direction.NONE)
        ])

        self.assertEqual(calc_floors(Direction.DOWN, 3, {
            Request(5, Direction.NONE),
            Request(1, Direction.NONE),
            Request(2, Direction.NONE),
            Request(4, Direction.NONE),
        }), [
            Request(2, Direction.NONE),
            Request(1, Direction.NONE),
            Request(4, Direction.NONE),
            Request(5, Direction.NONE)
        ])

        self.assertEqual(calc_floors(Direction.UP, 3, {
            Request(5, Direction.NONE),
            Request(1, Direction.NONE),
            Request(2, Direction.DOWN),
            Request(4, Direction.UP),
        }), [
            Request(4, Direction.UP),
            Request(5, Direction.NONE),
            Request(2, Direction.DOWN),
            Request(1, Direction.NONE)
        ])

        self.assertEqual(calc_floors(Direction.DOWN, 3, {
            Request(5, Direction.NONE),
            Request(1, Direction.NONE),
            Request(2, Direction.DOWN),
            Request(4, Direction.UP),
        }), [
            Request(2, Direction.DOWN),
            Request(1, Direction.NONE),
            Request(4, Direction.UP),
            Request(5, Direction.NONE)
        ])

        # the only case when we don't know direction
        self.assertEqual(calc_floors(Direction.NONE, 3, {
            Request(3, Direction.NONE)
        }), [Request(3, Direction.NONE)])

        self.assertEqual(calc_floors(Direction.UP, 3, {
            Request(3, Direction.UP),
            Request(3, Direction.DOWN),
        }), [
            Request(3, Direction.UP),
            Request(3, Direction.DOWN)
        ])


class TestCalcStates(unittest.TestCase):

    def test_calc_states(self):

        self.assertEqual(calc_states(3, [
            Request(3, Direction.UP)
        ]), [
            (State.OPEN, set()),
            (State.CLOSE, {Request(3, Direction.UP)})
        ])

        self.assertEqual(calc_states(3, [
            Request(5, Direction.UP)
        ]), [
            (State.UPWARD, set()),
            (State.UPWARD, set()),
            (State.OPEN, set()),
            (State.CLOSE, {Request(5, Direction.UP)})
        ])

        self.assertEqual(calc_states(3, [
            Request(1, Direction.UP)
        ]), [
            (State.DOWNWARD, set()),
            (State.DOWNWARD, set()),
            (State.OPEN, set()),
            (State.CLOSE, {Request(1, Direction.UP)})
        ])

        self.assertEqual(calc_states(3, [
            Request(3, Direction.UP),
            Request(3, Direction.DOWN),
            Request(3, Direction.NONE)
        ]), [
            (State.OPEN, set()),
            (State.CLOSE, {Request(3, Direction.DOWN), Request(3, Direction.UP), Request(3, Direction.NONE)})
        ])


class TestSchedulerImpl(unittest.TestCase):

    sc = SchedulerImpl()

    def test_scheduler(self):

        self.assertEqual(self.sc.calculate_route(CabinState(Direction.UP, 3, DoorState.CLOSED), {
            Request(5, Direction.NONE)
        }), [
            (State.UPWARD, set()),
            (State.UPWARD, set()),
            (State.OPEN, set()),
            (State.CLOSE, {Request(5, Direction.NONE)}),
        ])

        self.assertEqual(self.sc.calculate_route(CabinState(Direction.UP, 3, DoorState.OPEN), {
            Request(3, Direction.NONE),
            Request(1, Direction.DOWN),
            Request(4, Direction.UP)
        }), [
            (State.CLOSE, {Request(3, Direction.NONE)}),
            (State.UPWARD, set()),
            (State.OPEN, set()),
            (State.CLOSE, {Request(4, Direction.UP)}),
            (State.DOWNWARD, set()),
            (State.DOWNWARD, set()),
            (State.DOWNWARD, set()),
            (State.OPEN, set()),
            (State.CLOSE, {Request(1, Direction.DOWN)})
        ])

