from typing import List

from main.model import View, CabinState


class ViewImpl(View):

    def show_many(self, states: List[CabinState], tick: int):
        result = [""]*5
        for i, s in enumerate(states):
            lines = self.show(s, tick, "(Cabin %s)" % i)
            for j in range(5):
                result[j] = result[j] + lines[j]
        print("\n".join(result))

    def show(self, state: CabinState, tick: int, cabin_label: str = ""):
        text = [
            "------ Time %s %s -----" % (tick, cabin_label),
            "Floor: %s" % state.floor,
            "Direction: %s" % state.direction.value,
            "Doors: %s" % state.doors.value,
            ""
        ]
        return [s + " "*(len(text[0]) - len(s)) + "|" for s in text]
