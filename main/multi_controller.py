from collections import defaultdict
from typing import List, Set, Tuple

from main.model import Cabin, Request, Direction, View


class MultiController:

    def __init__(self,
                 cabins: List[Cabin],
                 view: View,
                 num_ticks):
        self._num_ticks = num_ticks
        self._cabins = cabins
        self._view = view
        self._cabins_num = len(cabins)
        self._internal_requests: defaultdict[int, Set[Tuple[int, Request]]] = defaultdict(set)
        self._external_requests: defaultdict[int, Set[Request]] = defaultdict(set)

    def internal_request(self, tick_number: int, floor: int, cabin_num: int):
        self._internal_requests[tick_number].add((cabin_num, Request(floor, Direction.NONE)))

    def external_request(self, tick_number: int, floor: int, direction: Direction):
        self._external_requests[tick_number].add(Request(floor, direction))

    def schedule_external(self, request: Request) -> int:
        min_waiting = 10**19
        cabin_idx = None
        for idx, cabin in enumerate(self._cabins):
            candidate = cabin.try_request(request)
            if candidate < min_waiting:
                min_waiting = candidate
                cabin_idx = idx
        return cabin_idx

    def run(self):
        for t in range(self._num_ticks):
            for i, r in self._internal_requests[t]:
                self._cabins[i].add_request(r)
            for r in self._external_requests[t]:
                i = self.schedule_external(r)
                self._cabins[i].add_request(r)

            for cbn in self._cabins:
                cbn.tick()
            self._view.show_many([c.state for c in self._cabins], t)

