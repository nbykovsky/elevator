from collections import deque
from typing import List, Set, Tuple

from main.model import Cabin, Scheduler, State, Request, Direction, DoorState, CabinState
from main.scheduler_impl import deduplicate_requests
from copy import deepcopy


class CabinImpl(Cabin):

    def __init__(self,
                 number_of_floors: int,
                 current_floor: int,
                 scheduler: Scheduler):
        if not (0 <= current_floor < number_of_floors):
            raise ValueError("Number of floors must be greater then current floor")
        self._number_of_floors = number_of_floors
        self._scheduler = scheduler
        self._state: CabinState = CabinState(Direction.NONE, current_floor, DoorState.CLOSED)
        self._requests: Set[Request] = set()
        self._route: List[(State, Set[Request])] = []  # opposite direction

    @property
    def state(self) -> CabinState:
        return self._state

    @staticmethod
    def find_spot(request: Request, route: List[Tuple[State, Set[Request]]]) -> int:
        for i, (s, rs) in enumerate(reversed(route)):
            if request in rs:
                return i
        raise RuntimeError("Request is not on the road")

    def _validate_request(self, request: Request):
        if not (0 <= request.floor < self._number_of_floors):
            raise ValueError("Wrong floor {}".format(request.floor))

    def _apply_request(self, request: Request) -> Tuple[List[Tuple[State, Set[Request]]], CabinState, Set[Request]]:

        state = deepcopy(self._state)

        if state.direction == Direction.NONE:
            if state.floor == request.floor:
                state.direction = request.direction
            else:
                state.direction = Direction.UP if state.floor < request.floor else Direction.DOWN

        requests = deepcopy(self._requests)
        requests.add(request)
        route = list(reversed(self._scheduler.calculate_route(state, requests)))
        return route, state, requests

    def add_request(self, request: Request):
        """
        Here we add request to the elevator
        """
        self._validate_request(request)
        route, self._state, self._requests = self._apply_request(request)
        self._route = route

    def try_request(self, request: Request) -> int:
        self._validate_request(request)
        route, state, requests = self._apply_request(request)
        return self.find_spot(request, route)

    def _up_action(self):
        self._state.direction = Direction.UP
        self._state.floor += 1

    def _down_action(self):
        self._state.direction = Direction.DOWN
        self._state.floor -= 1

    def _open_action(self):
        self._state.doors = DoorState.OPEN

    def _close_action(self):
        self._state.doors = DoorState.CLOSED

    def tick(self):

        if not self._route:
            return False

        step, closed_requests = self._route.pop()

        self._requests -= closed_requests

        {
            State.UPWARD: self._up_action,
            State.DOWNWARD: self._down_action,
            State.OPEN: self._open_action,
            State.CLOSE: self._close_action
        }[step]()

        return True

