from typing import List, Set, NamedTuple, Tuple
from enum import Enum
from abc import ABC, abstractmethod
from dataclasses import dataclass


class Direction(Enum):
    UP = "up"
    DOWN = "down"
    NONE = "none"

    @classmethod
    def reverse(cls, direction):
        if direction == cls.UP:
            return cls.DOWN
        elif direction == cls.DOWN:
            return cls.UP
        else:
            return cls.NONE

    @classmethod
    def from_str(cls, s):
        if s == "UP":
            return cls.UP
        elif s == "DOWN":
            return cls.DOWN
        else:
            raise ValueError("Must be UP or DOWN")


class Request(NamedTuple):
    floor: int
    direction: Direction

    @property
    def dual(self):
        return Request((-1)*self.floor, Direction.reverse(self.direction))


class State(Enum):
    UPWARD = "upward"
    DOWNWARD = "downward"
    OPEN = "open"
    CLOSE = "close"


class DoorState(Enum):
    OPEN = "open"
    CLOSED = "closed"


@dataclass
class CabinState:
    direction: Direction
    floor: int
    doors: DoorState


class Scheduler(ABC):

    @staticmethod
    def of():
        from main.scheduler_impl import SchedulerImpl
        return SchedulerImpl()

    @abstractmethod
    def calculate_route(self,
                        state: CabinState,
                        requests: Set[Request]) -> List[Tuple[State, Set[Request]]]:
        pass



class Cabin(ABC):

    @staticmethod
    def of(number_of_floors: int, current_floor: int, scheduler: Scheduler):
        from main.cabin_impl import CabinImpl
        return CabinImpl(number_of_floors, current_floor, scheduler)

    @abstractmethod
    def state(self) -> CabinState:
        pass

    @abstractmethod
    def add_request(self, request: Request):
        pass

    @abstractmethod
    def try_request(self, request: Request) -> int:
        pass

    @abstractmethod
    def tick(self):
        pass


class View(ABC):

    @staticmethod
    def of():
        from main.view_impl import ViewImpl
        return ViewImpl()

    @abstractmethod
    def show(self, state: CabinState, tick: int):
        pass

    @abstractmethod
    def show_many(self, states: List[CabinState], tick: int):
        pass
