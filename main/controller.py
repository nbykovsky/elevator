from collections import defaultdict
from typing import List, Dict, Set

from main.model import Cabin, Request, View, Direction


class Controller:

    def __init__(self,
                 cabin: Cabin,
                 view: View,
                 num_ticks: int,
                 ):
        self._cabin = cabin
        self._requests: defaultdict[int, Set[Request]] = defaultdict(set)
        self._view = view
        self._num_ticks = num_ticks

    def internal_request(self, tick_number: int, floor: int):
        self._requests[tick_number].add(Request(floor, Direction.NONE))

    def external_request(self, tick_number: int, floor: int, direction: Direction):
        self._requests[tick_number].add(Request(floor, direction))

    def run(self):
        for t in range(self._num_ticks):
            for r in self._requests[t]:
                self._cabin.add_request(r)
            self._cabin.tick()
            print("\n".join(self._view.show(self._cabin.state, t)))
