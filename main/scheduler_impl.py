from typing import List, Tuple, Set

from main.model import Scheduler, State, Request, Direction, DoorState, CabinState


def deduplicate_requests(requests: Set[Request]) -> Set[Request]:
    """
    Removes requests which will not affect behaviour
    ex. if we have Request(5, UP) and Request(5, NONE), Request(5, NONE) could be
    safely deleted. But if we have Request(5, UP) and Request(5, DOWN), none of those could be deleted
    """
    out = set()
    for r in requests:
        if r.direction == Direction.NONE and \
                ({Request(r.floor, Direction.UP), Request(r.floor, Direction.DOWN)} & requests):
            continue
        else:
            out.add(r)
    return out


def calc_one_way(req: List[Request], current_floor: int) -> List[Request]:
    route: List[Request] = []
    route.extend([r for r in req if r.floor >= current_floor and r.direction != Direction.DOWN])
    route.extend([r for r in reversed(req) if r.direction == Direction.DOWN
                  or r.floor < current_floor and r.direction == Direction.NONE])
    route.extend([r for r in req if r.floor < current_floor and r.direction == Direction.UP])
    return route


def calc_floors(
        current_direction: Direction,
        current_floor: int,
        requests: Set[Request]) -> List[Request]:
    """
    Orders requests by the time of its fulfillment.
    Contract!!! : works correctly only if all requests are actionable meaning each removal
    from requests will lead to different behaviour. For example the following state
    requests={Request(5, UP), Request(5, NONE)} is invalid because Request(5, NONE) could be removed
    without any change is behaviour
    """

    if current_direction == Direction.NONE:
        if list(filter(lambda r: r != Request(current_floor, Direction.NONE), requests)):
            raise ValueError("Wrong State")
        return [Request(current_floor, Direction.NONE)]

    req = sorted(list(requests), key=lambda x: x.floor)

    if current_direction == Direction.UP:
        return calc_one_way(req, current_floor)
    else:
        return [r1.dual for r1 in calc_one_way([r0.dual for r0 in reversed(req)], -current_floor)]


def calc_states(current_floor: int, stops: List[Request]) -> List[Tuple[State, Set[Request]]]:

    # grouping requests by floor
    grouped: List[Tuple[int, Set[Request]]] = [(stops[0].floor, {stops[0]})]
    for r in stops[1:]:
        if r.floor == grouped[-1][0]:
            grouped[-1][1].add(r)
        else:
            grouped.append((r.floor, {r}))

    commands: List[Tuple[State, Set[Request]]] = []
    for floor, rs in grouped:
        action = State.UPWARD if floor > current_floor else State.DOWNWARD
        for _ in range(abs(floor - current_floor)):
            commands.append((action, set()))

        commands.append((State.OPEN, set()))
        commands.append((State.CLOSE, rs))
        current_floor = floor

    return commands


class SchedulerImpl(Scheduler):

    def calculate_route(self,
                        state: CabinState,
                        requests: Set[Request]) -> List[Tuple[State, Set[Request]]]:
        stops = calc_floors(state.direction, state.floor, requests)
        states = calc_states(state.floor, stops)
        if not states:
            raise RuntimeError("States must me not empty")
        # if doors open that must be request on the same floor
        if state.doors == DoorState.OPEN:
            del states[0]
        return states
